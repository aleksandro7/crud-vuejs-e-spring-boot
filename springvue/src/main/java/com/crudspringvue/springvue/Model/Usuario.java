package com.crudspringvue.springvue.Model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

//import org.springframework.data.annotation.Id;

@Entity
public class Usuario implements Serializable  {
	
	@javax.persistence.Id
	@GeneratedValue
    private long userId;
    private String nome;
    private String ultimo;
    private int idade;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUltimo() {
		return ultimo;
	}

	public void setUltimo(String ultimo) {
		this.ultimo = ultimo;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}
    
	
}